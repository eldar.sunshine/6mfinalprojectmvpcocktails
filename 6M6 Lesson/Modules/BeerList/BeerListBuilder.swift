//
//  BeerListBuilder.swift
//  6M6 Lesson
//
//  Created by Эльдар on 27/3/23.
//

import UIKit

class BeerListBuilder {
    static func build() -> UIViewController {
        let vc = BeerListVC()
        let presenter = BeerListPresenter(view: vc)
        vc.presenter = presenter
        return vc
    }
}

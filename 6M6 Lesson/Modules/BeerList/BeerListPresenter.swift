//
//  BeerListPresenter.swift
//  6M6 Lesson
//
//  Created by Эльдар on 27/3/23.
//

import Foundation

protocol BeerListPresenterDelegate: AnyObject {
    init(view: BeerListDelegate)
    func getBeerList()
}

class BeerListPresenter {
  
    private weak var view: BeerListDelegate?
    
    private var service = BeerListService()
    
    required init(view: BeerListDelegate) {
        self.view = view
    }
}

extension BeerListPresenter: BeerListPresenterDelegate {
  
    func getBeerList() {
//        service.getBeer { model
//            if model != nil {
//                view?.recieveBeer(beers: [BeerElement(id: 1, name: "cocktail", tagline: "123", firstBrewed: "23", description: "super cocktail", imageURL: "https://", abv: 1.2, ibu: 1.3, targetFg: 1, targetOg: 1.2, ebc: 1, srm: 1.2, ph: 1.2, attenuationLevel: 1.2, volume: nil, boilVolume: nil, method: nil, ingredients: nil, foodPairing: nil, brewersTips: nil, contributedBy: nil)])
//            } else {
//                view?.recieveError(error: <#T##Error#>)
//            }
//        }
        
        view?.recieveBeer(beers: [BeerElement(id: 1, name: "cocktail", tagline: "123", firstBrewed: "23", description: "super cocktail", imageURL: "https://", abv: 1.2, ibu: 1.3, targetFg: 1, targetOg: 1.2, ebc: 1, srm: 1.2, ph: 1.2, attenuationLevel: 1.2, volume: nil, boilVolume: nil, method: nil, ingredients: nil, foodPairing: nil, brewersTips: nil, contributedBy: nil)])
    }
    
    
    
}

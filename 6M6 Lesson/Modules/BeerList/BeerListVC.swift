//
//  BeerListVC.swift
//  6M6 Lesson
//
//  Created by Эльдар on 27/3/23.
//

import UIKit

protocol BeerListDelegate: AnyObject {
    func recieveBeer(beers: [BeerElement])
    func recieveError(error: Error)
}

final class BeerListVC: UIViewController {
    var presenter: BeerListPresenterDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.getBeerList()
    }
}

extension BeerListVC: BeerListDelegate {
    func recieveError(error: Error) {
        print(error.localizedDescription)
    }
    
    func recieveBeer(beers: [BeerElement]) {
        print(beers)
    }
    
    
}

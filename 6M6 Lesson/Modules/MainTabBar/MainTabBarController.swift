//
//  MainTabBarController.swift
//  6M6 Lesson
//
//  Created by Эльдар on 27/3/23.
//

import UIKit

class MainTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        configureTabBarItems()
    }
    
    
    private func configureTabBarItems() {
        
        let beerVC = BeerListBuilder.build()
        beerVC.tabBarItem = UITabBarItem(title: "Beer list", image: UIImage(systemName: "1.circle"), tag: 0)
        
        let randomVC = RandomBeerVC()
        randomVC.tabBarItem = UITabBarItem(title: "Beer Random", image: UIImage(systemName: "2.circle"), tag: 1)
        
        let searchVC = SearchVC()
        searchVC.tabBarItem = UITabBarItem(title: "Beer Search", image: UIImage(systemName: "3.circle"), tag: 2)
        
        let listNavigationVC = UINavigationController(rootViewController: beerVC)
        
        let randomNavigationVC = UINavigationController(rootViewController: randomVC)
        
        let searchNavigationVC = UINavigationController(rootViewController: searchVC)
        
        setViewControllers([listNavigationVC, randomNavigationVC, searchNavigationVC], animated: true)
    }
}
